const path = require('path');
const rootPath = __dirname;


const env = process.env.NODE_ENV;

let databaseUrl = 'mongodb://localhost/shop';
let port = 8000;

if (env === "test") {
  databaseUrl = 'mongodb://localhost/shop_test';
  port = 8010;
}

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  port,
  db: {
    url: databaseUrl,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
  facebook: {
    appId: process.env.FACEBOOK_APP_ID,
    appSecret: process.env.FACEBOOK_APP_SECRET,
  },
  google: {
    clientId: process.env.GOOGLE_CLIENT_ID,
  },
  url: `http://localhost:${port}/uploads/`
};


