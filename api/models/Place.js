const mongoose = require('mongoose');

const PlaceSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  reviews: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Review',
  }],

  photos: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Photo',
  }],

  title: {
    type: String,
    required: [true, 'Please add title'],
  },

  description: {
    type: String,
    required: [true, 'Please add description'],
  },

  mainPhoto: {
    type: String,
    required: [true, 'Please add photo']
  },

  overallRating: {
    type: mongoose.Decimal128,
    default: 0
  },
  qualityRating: {
    type: mongoose.Decimal128,
    default: 0
  },
  qualityRatingNum: {
    type: Number,
    default: 0
  },
  serviceRating: {
    type: mongoose.Decimal128,
    default: 0
  },
  serviceRatingNum: {
    type: Number,
    default: 0

  },
  interiorRating: {
    type: mongoose.Decimal128,
    default: 0
  },
  interiorRatingNum: {
    type: Number,
    default: 0

  },
});

const Place = mongoose.model('Place', PlaceSchema);
module.exports = Place;
