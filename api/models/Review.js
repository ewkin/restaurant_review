const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },

  place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Place',
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  review: {
    type: String,
    required: true,
  },
  qualityRating: {
    type: Number,
    required: true
  },
  serviceRating: {
    type: Number,
    required: true
  },
  interiorRating: {
    type: Number,
    required: true
  },

});

const Review = mongoose.model('Review', ReviewSchema);
module.exports = Review;
