const mongoose = require('mongoose');

const PhotoSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Place',
    required: true
  },

  photoComment: {
    type: String,
    required: true
  },

});

const Photo = mongoose.model('Photo', PhotoSchema);
module.exports = Photo;
