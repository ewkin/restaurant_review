const express = require('express');
const Photo = require('../models/Photo');
const config = require('../config');
const auth = require("../middleware/auth");
const Place = require("../models/Place");
const upload = require('../multer').photoComment;


const router = express.Router();


router.get('/:id', async (req, res) => {
  try {
    const photos = await Photo.find({place: req.params.id});

    return res.status(200).send(photos);

  } catch (e) {
    return res.status(400).send({message: e.message});
  }
});

router.post('/', upload.single('photoComment'), auth, async (req, res) => {
  try {
    const photo = new Photo({
      user: req.user._id,
      place: req.body.place,
      photoComment: req.file ? config.url + req.file.filename : null,
    });

    const place = await Place.findById(req.body.place).populate('reviews');
    place.photos.push(photo);

    await photo.save();
    await place.save();
    return res.send(photo);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    if (req.user.role !== 'admin') {
      return res.status(401).send({error: 'No permission'});
    }
    const photos = await Photo.findByIdAndDelete(req.params.id);

    return res.status(200).send(photos);

  } catch (e) {
    return res.status(400).send({message: e.message});
  }
});


module.exports = router;