const express = require('express');
const Review = require('../models/Review');
const Place = require('../models/Place');
const auth = require("../middleware/auth");


const router = express.Router();


router.get('/:id', async (req, res) => {
  try {
    const reviews = await Review.find({place: req.params.id}).populate('user', 'displayName');

    return res.status(200).send(reviews);

  } catch (e) {
    return res.status(400).send({message: e.message});
  }
});

router.delete('/:id/:placeId', auth, async (req, res) => {
  try {
    const review = await Review.findByIdAndDelete(req.params.id);

    const place = await Place.findById(req.params.placeId).populate('reviews');

    let totalQualityRating = 0;
    let totalServiceRating = 0;
    let totalInteriorRating = 0;
    place.reviews.map((obj) => {
      totalInteriorRating += obj.interiorRating;
      totalServiceRating += obj.serviceRating;
      totalQualityRating += obj.qualityRating;
    });

    if (review.qualityRating !== 0) {
      place.qualityRatingNum = place.qualityRatingNum - 1;
      place.qualityRating = (totalQualityRating - review.qualityRating) / place.qualityRatingNum !== 0 ? place.qualityRatingNum : 0;
    }

    if (review.serviceRating !== 0) {
      place.serviceRatingNum = place.serviceRatingNum - 1;
      place.serviceRating = (totalQualityRating - review.serviceRating) / place.serviceRatingNum !== 0 ? place.serviceRatingNum : 0;
    }
    if (review.interiorRating !== 0) {
      place.interiorRatingNum = place.interiorRatingNum - 1;
      place.interiorRating = (totalQualityRating - review.interiorRating) / place.interiorRatingNum !== 0 ? place.interiorRatingNum : 0;
    }
    place.overallRating = (parseFloat(place.interiorRating) + parseFloat(place.serviceRating) + parseFloat(place.qualityRating)) / 3
    place.reviews.push(review);
    place.save();

    return res.status(200).send(review);

  } catch (e) {
    return res.status(400).send({message: e.message});
  }
});

router.post('/', auth, async (req, res) => {
  try {
    const review = new Review({
      user: req.user._id,
      place: req.body.place,
      review: req.body.review,
      date: new Date(),
      qualityRating: req.body.qualityRating,
      serviceRating: req.body.serviceRating,
      interiorRating: req.body.interiorRating
    });

    await review.save();

    const place = await Place.findById(req.body.place).populate('reviews');

    let totalQualityRating = 0;
    let totalServiceRating = 0;
    let totalInteriorRating = 0;
    place.reviews.map((obj) => {
      totalInteriorRating += obj.interiorRating;
      totalServiceRating += obj.serviceRating;
      totalQualityRating += obj.qualityRating;
    });

    if (req.body.qualityRating !== 0) {
      place.qualityRatingNum = place.qualityRatingNum + 1;
      place.qualityRating = (totalQualityRating + req.body.qualityRating) / place.qualityRatingNum;
    }

    if (req.body.serviceRating !== 0) {
      place.serviceRatingNum = place.serviceRatingNum + 1;
      place.serviceRating = (totalQualityRating + req.body.serviceRating) / place.serviceRatingNum;
    }
    if (req.body.interiorRating !== 0) {
      place.interiorRatingNum = place.interiorRatingNum + 1;
      place.interiorRating = (totalQualityRating + req.body.interiorRating) / place.interiorRatingNum;
    }
    place.overallRating = (parseFloat(place.interiorRating) + parseFloat(place.serviceRating) + parseFloat(place.qualityRating)) / 3
    place.reviews.push(review);
    place.save();

    return res.send(review);
  } catch (e) {
    return res.status(400).send(e);
  }
});


module.exports = router;