const express = require('express');
const Place = require('../models/Place');
const config = require('../config');
const auth = require("../middleware/auth");
const upload = require('../multer').mainPhoto;


const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const places = await Place.find();
    return res.send(places);
  } catch (e) {
    return res.status(400).send(e);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const place = await Place.findById(req.params.id).populate(
        'reviews photos'
      )
    ;
    return res.status(200).send(place);

  } catch (e) {
    return res.status(400).send({message: e.message});
  }
});

router.delete('/:id/:ownerId', auth, async (req, res) => {
  try {
    if (req.user.role === 'admin' || req.params.ownerId.toString() === req.user._id.toString()) {
      const place = await Place.findByIdAndDelete(req.params.id);
      return res.status(200).send(place);
    }

    return res.status(401).send({error: 'No permission'});

  } catch (e) {
    return res.status(400).send({message: e.message});
  }
});

router.post('/', upload.single('mainPhoto'), auth, async (req, res) => {
  try {
    if (!JSON.parse(req.body.consent)) {
      return res.status(401).send({consent: 'You need to agree to conditions'});
    }
    const place = new Place({
      user: req.user._id,
      title: req.body.title,
      description: req.body.description,
      mainPhoto: req.file ? config.url + req.file.filename : null,
    });

    await place.save();
    return res.send(place);
  } catch (e) {
    return res.status(400).send(e);
  }
});


module.exports = router;