const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Place = require("./models/Place");
const Review = require("./models/Review");
const Photo = require("./models/Photo");


const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);
  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@app',
    password: '1qaz@WSX29',
    token: nanoid(),
    role: 'user',
    displayName: "John"
  }, {
    email: 'admin@app',
    password: '1qaz@WSX29',
    token: nanoid(),
    role: 'admin',
    displayName: "Mike"
  });


  const [noName, redCow] = await Place.create({
      user: user,
      _id: "507f1f77bcf86cd799439011",
      reviews: [],
      photos: [],
      title: 'No Name',
      description: "A nice bar",
      mainPhoto: config.url + 'fixtures/noName.jpg',
      overallRating: 5,
      qualityRating: 5,
      qualityRatingNum: 1,
      serviceRating: 5,
      serviceRatingNum: 1,
      interiorRating: 5,
      interiorRatingNum: 1
    },
    {
      user: admin,
      reviews: [],
      photos: [],
      title: 'Red Cow',
      description: "A nice bar",
      mainPhoto: config.url + 'fixtures/redCow.jpg',
      overallRating: 5,
      qualityRating: 5,
      qualityRatingNum: 1,
      serviceRating: 5,
      serviceRatingNum: 1,
      interiorRating: 5,
      interiorRatingNum: 1
    }
  );
  const [noNameRev, redCowRev] = await Review.create({
      user: admin,
      place: noName,
      date: new Date(),
      review: 'Good bar for expats',
      qualityRating: 5,
      serviceRating: 5,
      interiorRating: 5,
    },
    {
      user: user,
      place: redCow,
      date: new Date(),
      review: 'Good steaks',
      qualityRating: 5,
      serviceRating: 5,
      interiorRating: 5,
    }
  );
  const [noNamePhoto, redCowPhoto] = await Photo.create({
      user: user,
      place: noName,
      photoComment: config.url + 'fixtures/redCowPic.jpg',
    },
    {
      user: user,
      place: redCow,
      photoComment: config.url + 'fixtures/noNamePic.jpg',
    }
  );

  await noName.reviews.push(noNameRev);
  await redCow.reviews.push(redCowRev);
  await noName.photos.push(noNamePhoto);
  await redCow.photos.push(redCowPhoto);
  await noName.save();
  await redCow.save();


  await mongoose.connection.close();

};

run().catch(console.error);