const {I} = inject();

const users = {
  'user@app': '1qaz@WSX29'
}

Given('я залогинен как пользователь {string}', (email) => {
  I.amOnPage('/login');
  I.fillField('email', email);
  I.fillField('password', users[email]);
  I.click(`//button//*[contains(text(),"Sign in")]/..`);
  I.see("Login successful");
});