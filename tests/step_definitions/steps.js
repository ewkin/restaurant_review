const {I} = inject();


Given('я нахожусь на странице логина', () => {
  I.amOnPage('/login');
});

When('я ввожу в поля текст:', table => {
  for (const id in table.rows) {
    if (id < 1) {
      continue; // skip a header of a table
    }
    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

When('нажимаю на кнопку {string}', (name) => {
  I.click(`//button//*[contains(text(),"${name}")]/..`);
});

Then('вижу {string}', (name) => {
  I.see(name);
});


Then('попадаю на страницу списка всех мест', () => {
  I.amOnPage('/');
});

Then('нажимаю на кнопку {string}', (name) => {
  I.click(`//button//*[contains(text(),"${name}")]/..`);
});

Then('нажимаю на пункт меню {string}', (name) => {
  I.click(`//ul//*[contains(text(),"${name}")]/.`);
});

Then('попадаю на страницу добавления места места', () => {
  I.amOnPage('/add_place');
});

When('я ввожу в поля текст:', table => {
  for (const id in table.rows) {
    if (id < 1) {
      continue; // skip a header of a table
    }
    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

Then('я загружаю картинку', () => {
  I.attachFile('form input[name=mainPhoto]', '.././api/public/uploads/fixtures/noName.jpg');
});

Then('соглашаюсь с правилами', () => {
  I.checkOption("input[type='checkbox']");
});


Then('нажимаю на кнопку {string}', (name) => {
  I.click(`//button//*[contains(text(),"${name}")]/..`);
});


Then('попадаю на страницу добавления отзыва', () => {
  I.amOnPage('/place/507f1f77bcf86cd799439011');
});

Then('я ставлю рейтинг {string}', (name) => {
  I.click(`label[for='${name}']`);
});

Then('скролю вниз', () => {
  I.scrollPageToBottom();
})

Then('вижу нотифаер и среднее количесво звезд {string}', (name) => {
  I.see(name);
  I.seeElement("span[aria-label='4 Stars']")
});