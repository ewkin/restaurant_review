import React, {useEffect} from 'react';
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPlacesRequest} from "../../store/actions/placesActions";
import OnePlace from "./OnePlace";


const Places = () => {
  const dispatch = useDispatch();
  const places = useSelector(state => state.places.places);

  useEffect(() => {
    dispatch(fetchPlacesRequest());
  }, [dispatch]);

  return (
    <Grid container spacing={1}>
      <Grid item xs={12}>
        <h2>Places</h2>
      </Grid>
      {places.map(place => <OnePlace key={place._id} place={place}/>)}
    </Grid>
  );
};

export default Places;