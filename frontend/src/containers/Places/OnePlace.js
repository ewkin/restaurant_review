import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";
import {Rating} from "@material-ui/lab";
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import {useDispatch} from "react-redux";
import {historyPush} from "../../store/actions/historyActions";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  photos: {
    display: 'flex',
    alignItems: 'center'
  },
  photoText: {
    paddingTop: '5px'
  }
});

const OnePlace = ({place}) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <Grid item xs={6} sm={4} md={3}>
      <Card className={classes.root}>
        <CardActionArea onClick={() => dispatch(historyPush(`/place/${place._id}`))}>
          <CardMedia
            className={classes.media}
            image={place.mainPhoto}
            title="Cafe photo"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {place.title}
            </Typography>
          </CardContent>
          <CardActions>
            <Grid container>
              <Grid item xs={12}>
                <Rating name="read-only"
                        value={place.overallRating.$numberDecimal ? parseFloat(place.overallRating.$numberDecimal) : 0}
                        readOnly/>
              </Grid>
              <Grid item xs={12}>
                <Typography gutterBottom variant="body1" component="h2">
                  ({place?.overallRating.$numberDecimal && place.overallRating.$numberDecimal.substring(0, 4) + ", "}{place.reviews.length} reviews)
                </Typography>
              </Grid>
              <Grid item xs={12} className={classes.photos}>
                <PhotoCameraIcon/>
                <Typography gutterBottom variant="body1" component="h2" className={classes.photoText}>
                  {place.photos.length} photos
                </Typography>
              </Grid>
            </Grid>
          </CardActions>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

export default OnePlace;