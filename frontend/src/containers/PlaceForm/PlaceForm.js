import React, {useState} from 'react';
import {Checkbox, FormControlLabel, Grid, makeStyles} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/Form/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {createPlaceRequest} from "../../store/actions/placesActions";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const PlaceForm = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [place, setPlace] = useState({
    title: '',
    description: '',
    mainPhoto: null,
    consent: false
  });

  const error = useSelector(state => state.places.createPlaceError);
  const loading = useSelector(state => state.places.createPlaceLoading);


  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setPlace(prev => ({...prev, [name]: value}));
  };
  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(createPlaceRequest({...place}));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setPlace(prevState => ({
      ...prevState, [name]: value
    }));
  };

  return (
    <Grid container spacing={1}>
      <Grid item>
        <h2>Add a new place</h2>
      </Grid>

      <Grid item container spacing={1} direction={"row"} component={"form"} onSubmit={submitFormHandler}>
        <Grid item xs={2}>
          <p>Title</p>
        </Grid>
        <FormElement label={'Title'}
                     required
                     size={10}
                     onChange={inputChangeHandler}
                     name={'title'}
                     type={'text'}
                     value={place.title}
                     error={getFieldError('title')}
        />
        <Grid item xs={2}>
          <p>Description</p>
        </Grid>
        <FormElement label={'Description'}
                     required
                     size={10}
                     type="text"
                     rows={4}
                     onChange={inputChangeHandler}
                     name={'description'}
                     value={place.description}
                     multiline
                     error={getFieldError('description')}
        />
        <Grid item xs={2}>
          <p>Main photo</p>
        </Grid>
        <Grid item xs={10}>
          <FileInput
            name='mainPhoto'
            label='Photo'
            onChange={fileChangeHandler}
            error={getFieldError('mainPhoto')}

          />
        </Grid>
        <Grid item xs={8} sm={9}>
          <p>By submitting this form, you agree that the following information will be submitted to the public domain,
            and administration of this site will have full control over the above mentioned information</p>
        </Grid>
        <Grid item xs={4} sm={3}>
          <FormControlLabel
            control={
              <Checkbox
                checked={place.consent}
                onChange={() => setPlace({...place, consent: !place.consent})}
                name="check"
                color="secondary"
              />
            }
            label="I agree"/>
        </Grid>
        <Grid item xs>
          <ButtonWithProgress
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            loading={loading}
            disabled={loading || !place.consent}
          >
            Submit new place
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default PlaceForm;