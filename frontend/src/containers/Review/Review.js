import React from 'react';
import {Grid, Typography} from "@material-ui/core";
import {Rating} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const Review = ({review, admin, onDelete}) => {
    const date = new Date(review.date)
    return (
      <Grid item container xs={12}>
        <Grid item xs={12}>
          <p>On {date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()}, {review.user.displayName} said</p>
          <p>{review.review}</p>
        </Grid>
        <Grid item xs={2}>
          <Typography display={"inline"} component="legend">Food quality</Typography>
          <Rating name="read-only" value={review.qualityRating} readOnly/>
        </Grid>
        <Grid item xs={2}>
          <Typography component="legend">Service quality</Typography>
          <Rating name="read-only" value={review.serviceRating} readOnly/>
        </Grid>
        <Grid item xs={2}>
          <Typography component="legend">Interior</Typography>
          <Rating name="read-only" value={review.interiorRating} readOnly/>
        </Grid>
        {admin ?
          <Grid item xs={6}>
            <ButtonWithProgress
              type="submit"
              fullWidth
              onClick={() => onDelete(review._id)}
              variant="contained"
              color="inherit"
            >
              Delete
            </ButtonWithProgress>
          </Grid> : null}
      </Grid>
    )
      ;
  }
;

export default Review;