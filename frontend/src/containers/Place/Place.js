import React, {useEffect, useState} from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useParams} from "react-router-dom";
import {deletePlaceRequest, fetchPlaceRequest} from "../../store/actions/placesActions";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import {Rating} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {createReviewRequest, deleteReviewRequest, fetchReviewsRequest} from "../../store/actions/reviewActions";
import Review from "../Review/Review";
import FileInput from "../../components/UI/Form/FileInput";
import {createPhotoRequest, deletePhotoRequest} from "../../store/actions/photosActions";
import DialogContainer from "../../components/UI/DialogContainer/DialogContainer";


const useStyles = makeStyles(theme => ({
  linedContainer: {
    borderBottom: '1px solid #A0A0A0'
  },
  imageContainer: {
    overflow: "hidden",
    maxHeight: '340px',
    "&$imageContainer img": {
      width: "100%",
      height: "auto",
      display: 'block'
    },
    "&$imageContainer svg": {
      width: "100%",
      height: "auto",
    },
  },
  submit: {
    margin: theme.spacing(1, 0, 2)
  },
}));


const Place = () => {
  const classes = useStyles();
  const {id} = useParams()
  const dispatch = useDispatch();
  const place = useSelector(state => state.places.place);
  const reviews = useSelector(state => state.reviews.reviews);
  const user = useSelector(state => state.users.user);

  const error = useSelector(state => state.places.createPlaceError);
  const loading = useSelector(state => state.places.createPlaceLoading);
  const commented = reviews.find(review => review.user._id === user?._id)

  const [dialog, setDialog] = useState({open: false, url: '', id: ''});

  const [review, setReview] = useState({
    qualityRating: 0,
    serviceRating: 0,
    interiorRating: 0,
    review: '',
    place: id
  });

  const [photo, setPhoto] = useState({
    photoComment: null,
    place: id
  });


  useEffect(() => {
    dispatch(fetchPlaceRequest(id));
    dispatch(fetchReviewsRequest(id))
  }, [dispatch, id]);

  const handleChange = () => {
    setDialog({...dialog, open: false, url: '', id: ''});
  };


  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setReview(prev => ({...prev, [name]: value}));
  };
  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(createReviewRequest({...review}));
    setReview({
      qualityRating: 0,
      serviceRating: 0,
      interiorRating: 0,
      review: '',
      place: id
    })
  };

  const sendPhoto = () => {
    dispatch(createPhotoRequest({...photo}));
    setPhoto({...photo, photoComment: null})

  }

  const deleteReview = (reviewId) => {
    dispatch(deleteReviewRequest({review: reviewId, place: id}));
  };

  const deletePhoto = (photoId) => {
    dispatch(deletePhotoRequest({photoId: photoId, place: id}));
  };
  const deletePlace = () => {
    dispatch(deletePlaceRequest({placeId: place._id, userId: place.user}));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setPhoto(prevState => ({
      ...prevState, [name]: value
    }));
  };


  return (
    <Grid container spacing={4}>
      <Grid item container xs={12} spacing={1}>
        <Grid item xs={8}>
          <Typography variant="h4" gutterBottom>{place?.title}</Typography>
          <Typography variant="body1" gutterBottom>{place?.description}</Typography>
        </Grid>
        <Grid item xs={4} className={classes.imageContainer}>
          <img src={place?.mainPhoto} alt={place?.title}/>
        </Grid>
      </Grid>
      {place ? place.photos.length > 0 ?
        <Grid item container xs={12} spacing={1} className={classes.linedContainer}>
          <Grid item xs={12}>
            <Typography variant="h5" gutterBottom>Gallery</Typography>
          </Grid>
          {place ? place.photos.map(photo =>
            <Grid item xs={2} key={photo._id} className={classes.imageContainer}
                  onClick={() => setDialog({...dialog, open: true, url: photo?.photoComment, id: photo?._id})}>
              <img src={photo?.photoComment} alt={place?.title}/>
            </Grid>
          ) : null}
        </Grid> : null : null}
      {(user?.role === 'admin' || user?._id === place?.user) ?
        <Grid item xs={12} spacing={1} className={classes.linedContainer}>
          <ButtonWithProgress
            type="submit"
            fullWidth
            onClick={() => {
              deletePlace();
            }}
            variant="contained"
            color="inherit"
            className={classes.submit}
          >
            Delete place
          </ButtonWithProgress>

        </Grid> : null}
      <Grid item container xs={12} spacing={1} className={classes.linedContainer}>
        <Grid item xs={12}>
          <Typography variant="h5" gutterBottom>Rating</Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography display={"inline"} component="legend">Overall</Typography>
          <Rating name="read-only" value={parseFloat(place?.overallRating.$numberDecimal)} readOnly/>
          <Typography gutterBottom variant="body1" component="body1">
            {place?.overallRating.$numberDecimal ? place.overallRating.$numberDecimal.substring(0, 4):null}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography display={"inline"} component="legend">Food quality</Typography>
          <Rating name="read-only" value={parseFloat(place?.qualityRating.$numberDecimal)} readOnly/>
          <Typography gutterBottom variant="body1" component="body1">
            {place?.qualityRating.$numberDecimal ? place.qualityRating.$numberDecimal.substring(0, 4):null}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography component="legend">Service quality</Typography>
          <Rating name="read-only" value={parseFloat(place?.serviceRating.$numberDecimal)} readOnly/>
          <Typography gutterBottom variant="body1" component="body1">
            {place?.serviceRating.$numberDecimal ? place.serviceRating.$numberDecimal.substring(0, 4):null}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography component="legend">Interior</Typography>
          <Rating name="read-only" value={parseFloat(place?.interiorRating.$numberDecimal)} readOnly/>
          <Typography gutterBottom variant="body1" component="body1">
            {place?.interiorRating.$numberDecimal ? place.interiorRating.$numberDecimal.substring(0, 4):null}
          </Typography>
        </Grid>
      </Grid>
      {reviews.length > 0 ?
        <Grid item container xs={12} spacing={1} className={classes.linedContainer}>
          <Grid item xs={12}>
            <Typography variant="h5" gutterBottom>Reviews</Typography>
          </Grid>
          {reviews.map(review => <Review key={review._id} onDelete={deleteReview} admin={user?.role === 'admin'}
                                         review={review}/>)}
        </Grid> : null}
      {user ? <>
        {!commented ?
          <Grid item container xs={12} spacing={1} component={"form"} onSubmit={submitFormHandler}>
            <FormElement label={'Review'}
                         required
                         size={12}
                         type="text"
                         rows={4}
                         onChange={inputChangeHandler}
                         name={'review'}
                         value={review.review}
                         multiline
                         error={getFieldError('description')}
            />
            <Grid item xs={6} sm={2}>
              <Typography component="legend">Food quality</Typography>
              <Rating
                name="qualityRating"
                value={review.qualityRating}
                onChange={(event, newValue) => setReview({...review, qualityRating: newValue})}
              />
            </Grid>
            <Grid item xs={6} sm={2}>
              <Typography component="legend">Service quality</Typography>
              <Rating
                name="serviceRating"
                value={review.serviceRating}
                onChange={(event, newValue) => setReview({...review, serviceRating: newValue})}
              />
            </Grid>
            <Grid item xs={6} sm={2}>
              <Typography component="legend">Interior</Typography>
              <Rating
                name="interiorRating"
                value={review.interiorRating}
                onChange={(event, newValue) => setReview({...review, interiorRating: newValue})}
              />
            </Grid>
            <Grid item xs={6} sm={6}>
              <ButtonWithProgress
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                loading={loading}
                disabled={loading}
              >
                Leave review
              </ButtonWithProgress>
            </Grid>
          </Grid> :
          <Grid item container xs={12} spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h5" gutterBottom>You have commented, do you want to delete?</Typography>
            </Grid>
            <Review review={commented}/>
            <ButtonWithProgress
              type="submit"
              fullWidth
              onClick={() => deleteReview(commented._id)}
              variant="contained"
              color="inherit"
              className={classes.submit}
            >
              Delete
            </ButtonWithProgress>
          </Grid>}
        <Grid item container xs={12} spacing={1} className={classes.linedContainer}>
          <Grid item xs={3}>
            <ButtonWithProgress
              type="submit"
              fullWidth
              disabled={photo.photoComment === null}
              onClick={sendPhoto}
              variant="contained"
              color="secondary"
              className={classes.submit}
              loading={loading}
            >
              Send photo
            </ButtonWithProgress>
          </Grid>
          <Grid item xs={9}>
            <FileInput
              name='photoComment'
              label='Photo'
              onChange={fileChangeHandler}
              error={getFieldError('photoComment')}
            />
          </Grid>

        </Grid>
      </> : null}
      {dialog.open ?
        <DialogContainer style={{
          width: '500px', height: '340px', backgroundColor: '#FFFFFF',
          boxShadow: 'none',
        }} open={dialog.open} handleClose={handleChange}>
          <div className={classes.imageContainer}>
            <img src={dialog.url} alt={place?.title}/>
          </div>
          {user?.role === 'admin' ?
            <ButtonWithProgress
              type="submit"
              fullWidth
              onClick={() => {
                deletePhoto(dialog.id);
                handleChange()
              }}
              variant="contained"
              color="inherit"
              className={classes.submit}
            >
              Delete
            </ButtonWithProgress> : null}
        </DialogContainer> : null}
    </Grid>
  );
};

export default Place;