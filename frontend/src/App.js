import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Places from "./containers/Places/Places";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import {useSelector} from "react-redux";
import {Helmet} from "react-helmet";
import PlaceForm from "./containers/PlaceForm/PlaceForm";
import Place from "./containers/Place/Place";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props}/> : <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Layout>
      <Helmet titleTemplate="%s - Cafe critic"
              defaultTitle="Cafe critic"/>
      <Switch>
        <ProtectedRoute
          isAllowed={user}
          redirectTo='/login'
          path="/add_place"
          component={PlaceForm}
        />
        <Route path="/register" component={Register}/>l
        <Route path="/login" component={Login}/>
        <Route path="/" exact component={Places}/>
        <Route path="/place/:id" exact component={Place}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
  );
};

export default App;