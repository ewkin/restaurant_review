import {createSlice} from "@reduxjs/toolkit";

let name = 'photos';

const photoSlice = createSlice({
  name,
  initialState: {
    photos: [],
    photosLoading: false,
    createPhotoLoading: false,
    createPhotoError: null,
    deletePhotoLoading: false,
    deletePhotoError: null
  },
  reducers: {
    fetchPhotosRequest: state => {
      state.photosLoading = true;
    },
    fetchPhotosSuccess: (state, {payload: photos}) => {
      state.photosLoading = false;
      state.photos = photos;
    },
    fetchPhotosFailure: state => {
      state.photosLoading = false;
    },
    createPhotoRequest: state => {
      state.createPhotoLoading = true;
    },
    createPhotoSuccess: state => {
      state.createPhotoLoading = false;
    },
    createPhotoFailure: (state, {payload: error}) => {
      state.createPhotoLoading = false;
      state.createPhotoError = error;
    },
    deletePhotoRequest: state => {
      state.deletePhotoLoading = true;
    },
    deletePhotoSuccess: state => {
      state.deletePhotoLoading = false;
    },
    deletePhotoFailure: (state, {payload: error}) => {
      state.deletePhotoLoading = false;
      state.deletePhotoError = error;
    }
  }


});

export default photoSlice
