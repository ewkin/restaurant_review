import {createSlice} from "@reduxjs/toolkit";

let name = 'reviews';

const reviewSlice = createSlice({
  name,
  initialState: {
    reviews: [],
    reviewsLoading: false,
    createReviewLoading: false,
    createReviewError: null,
    deleteReviewLoading: false,
    deleteReviewError: null
  },
  reducers: {
    fetchReviewsRequest: state => {
      state.reviewsLoading = true;
    },
    fetchReviewsSuccess: (state, {payload: reviews}) => {
      state.reviewsLoading = false;
      state.reviews = reviews;
    },
    fetchReviewsFailure: state => {
      state.reviewsLoading = false;
    },
    createReviewRequest: state => {
      state.createReviewLoading = true;
    },
    createReviewSuccess: state => {
      state.createReviewLoading = false;
    },
    createReviewFailure: (state, {payload: error}) => {
      state.createReviewLoading = false;
      state.createReviewError = error;
    },
    deleteReviewRequest: state => {
      state.deleteReviewLoading = true;
    },
    deleteReviewSuccess: state => {
      state.deleteReviewLoading = false;
    },
    deleteReviewFailure: (state, {payload: error}) => {
      state.deleteReviewLoading = false;
      state.deleteReviewError = error;
    }
  }


});

export default reviewSlice
