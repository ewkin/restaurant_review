import {createSlice} from "@reduxjs/toolkit";

const name = 'places';
const placesSlice = createSlice({
  name,
  initialState: {
    places: [],
    place: null,
    placesLoading: false,
    placeLoading: false,
    createPlaceLoading: false,
    createPlaceError: null,
    deletePlaceLoading: false,
    deletePlaceError: null
  },
  reducers: {
    fetchPlacesRequest: state => {
      state.placesLoading = true;
    },
    fetchPlacesSuccess: (state, {payload: places}) => {
      state.placesLoading = false;
      state.places = places;
    },
    fetchPlacesFailure: state => {
      state.placesLoading = false;
    },
    fetchPlaceFailure: state => {
      state.placeLoading = false;
    },
    fetchPlaceRequest: state => {
      state.placeLoading = true;
    },
    fetchPlaceSuccess: (state, {payload: place}) => {
      state.placeLoading = false;
      state.place = place;
    },
    createPlaceRequest: state => {
      state.createPlaceLoading = true;
    },
    createPlaceSuccess: state => {
      state.createPlaceLoading = false;
    },
    createPlaceFailure: (state, {payload: error}) => {
      state.createPlaceLoading = false;
      state.createPlaceError = error;
    },
    deletePlaceRequest: state => {
      state.deletePlaceLoading = true;
    },
    deletePlaceSuccess: state => {
      state.deletePlaceLoading = false;
    },
    deletePlaceFailure: (state, {payload: error}) => {
      state.deletePlaceLoading = false;
      state.deletePlaceError = error;
    }
  }
});

export default placesSlice;