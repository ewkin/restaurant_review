const {createAction} = require("@reduxjs/toolkit");

export const historyPush = createAction('history/push');

