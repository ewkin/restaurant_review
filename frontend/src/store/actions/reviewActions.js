import reviewSlice from "../slices/reviewSlice";


export const {
  fetchReviewsRequest,
  fetchReviewsSuccess,
  fetchReviewsFailure,
  createReviewRequest,
  createReviewSuccess,
  createReviewFailure,
  deleteReviewRequest,
  deleteReviewSuccess,
  deleteReviewFailure
} = reviewSlice.actions;
