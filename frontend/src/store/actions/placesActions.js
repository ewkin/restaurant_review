import placesSlice from "../slices/placesSlice";


export const {
  fetchPlacesRequest,
  fetchPlacesSuccess,
  fetchPlacesFailure,
  fetchPlaceRequest,
  fetchPlaceSuccess,
  fetchPlaceFailure,
  createPlaceSuccess,
  createPlaceRequest,
  createPlaceFailure,
  deletePlaceSuccess,
  deletePlaceRequest,
  deletePlaceFailure
} = placesSlice.actions;