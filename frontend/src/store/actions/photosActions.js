import photoSlice from "../slices/photoSlice";


export const {
  fetchPhotosRequest,
  fetchPhotosSuccess,
  fetchPhotosFailure,
  createPhotoRequest,
  createPhotoSuccess,
  createPhotoFailure,
  deletePhotoRequest,
  deletePhotoSuccess,
  deletePhotoFailure
} = photoSlice.actions;
