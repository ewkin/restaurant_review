import {takeEvery} from "redux-saga/effects";
import {historyPush} from "../actions/historyActions";

const historySagas = history => {
  return [
    takeEvery(historyPush, function* ({payload}) {
      yield history.push(payload);
    })
  ];
};

export default historySagas;