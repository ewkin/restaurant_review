import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../../axiosApi";
import {
  createPlaceFailure,
  createPlaceRequest,
  createPlaceSuccess, deletePlaceRequest, deletePlaceSuccess, fetchPlaceFailure, fetchPlaceRequest,
  fetchPlacesFailure, fetchPlacesRequest,
  fetchPlacesSuccess, fetchPlaceSuccess
} from "../actions/placesActions";

export function* fetchPlaces() {
  try {
    const response = yield axiosApi.get('/places');
    yield put(fetchPlacesSuccess(response.data));
  } catch (e) {
    yield put(fetchPlacesFailure());
    NotificationManager.error('Could not fetch cafes')
  }
}


export function* fetchPlace({payload: placeId}) {
  try {
    const response = yield axiosApi.get('/places/' + placeId);
    yield put(fetchPlaceSuccess(response.data));
  } catch (e) {
    yield put(fetchPlaceFailure());
    NotificationManager.error('Could not fetch cafes')
  }
}

export function* createPlace({payload: placeData}) {
  try {
    const formData = new FormData();
    Object.keys(placeData).forEach(key => {
      formData.append(key, placeData[key]);
    });
    yield axiosApi.post('/places', formData);
    yield put(createPlaceSuccess());
    yield put(historyPush('/'));
    NotificationManager.success('Your cafe was posted')
  } catch (e) {
    if (e.response.data.consent) {
      NotificationManager.error(e.response.data.consent)
    } else {
      NotificationManager.error('Could not post your cafe')
    }
    yield put(createPlaceFailure(e.response.data));
  }
}

export function* deletePlace({payload: prop}) {
  try {
    const response = yield axiosApi.delete('/places/' + prop.placeId + '/' + prop.userId);
    yield put(deletePlaceSuccess(response.data));
    yield put(historyPush('/'));
    NotificationManager.success('Your cafe was deleted')
  } catch (e) {
    yield put(fetchPlaceFailure());
    NotificationManager.error('Could not fetch cafes')
  }
}

const placesSagas = [
  takeEvery(fetchPlacesRequest, fetchPlaces),
  takeEvery(fetchPlaceRequest, fetchPlace),
  takeEvery(createPlaceRequest, createPlace),
  takeEvery(deletePlaceRequest, deletePlace)
];

export default placesSagas;

