import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {NotificationManager} from "react-notifications";
import {
  createPhotoFailure,
  createPhotoRequest,
  createPhotoSuccess, deletePhotoRequest, deletePhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest, fetchPhotosSuccess
} from "../actions/photosActions";
import {fetchPlaceRequest} from "../actions/placesActions";


export function* fetchPhotos({payload: placeId}) {
  try {
    const response = yield axiosApi.get('/photos/' + placeId);
    yield put(fetchPhotosSuccess(response.data));
  } catch (e) {
    yield put(fetchPhotosFailure());
    NotificationManager.error('Could not fetch photos')
  }
}


export function* createPhoto({payload: photos}) {
  try {
    const formData = new FormData();
    Object.keys(photos).forEach(key => {
      formData.append(key, photos[key]);
    });
    yield axiosApi.post('/photos', formData);
    yield put(createPhotoSuccess());
    yield put(fetchPlaceRequest(photos.place));
    NotificationManager.success('Your photo was posted')
  } catch (e) {
    if (e.response.data.consent) {
      NotificationManager.error(e.response.data.consent)
    } else {
      NotificationManager.error('Could not post your photo')
    }
    yield put(createPhotoFailure(e.response.data));
  }
}

export function* deletePhotos({payload: prop}) {
  try {
    const response = yield axiosApi.delete('/photos/' + prop.photoId);
    yield put(deletePhotoSuccess(response.data));
    yield put(fetchPlaceRequest(prop.place));
    NotificationManager.success('Photo was deleted')
  } catch (e) {
    yield put(fetchPhotosFailure());
    NotificationManager.error('Could not fetch photos')
  }
}

const photosSagas = [
  takeEvery(fetchPhotosRequest, fetchPhotos),
  takeEvery(createPhotoRequest, createPhoto),
  takeEvery(deletePhotoRequest, deletePhotos)
];

export default photosSagas;