import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {NotificationManager} from "react-notifications";
import {
  createReviewFailure,
  createReviewRequest,
  createReviewSuccess,
  deleteReviewFailure,
  deleteReviewRequest,
  deleteReviewSuccess,
  fetchReviewsFailure,
  fetchReviewsRequest,
  fetchReviewsSuccess
} from "../actions/reviewActions";
import {fetchPlaceRequest} from "../actions/placesActions";

export function* fetchReviews({payload: id}) {
  try {
    const response = yield axiosApi.get('/reviews/' + id);
    yield put(fetchReviewsSuccess(response.data));
  } catch (e) {
    yield put(fetchReviewsFailure());
    NotificationManager.error('Could not fetch cafes')
  }
}


export function* createReview({payload: reviewData}) {
  try {
    yield axiosApi.post('/reviews', reviewData);
    yield put(createReviewSuccess());
    yield put(fetchReviewsRequest(reviewData.place));
    yield put(fetchPlaceRequest(reviewData.place));
    NotificationManager.success('Your review was posted')
  } catch (e) {
    if (e.response.data.consent) {
      NotificationManager.error(e.response.data.consent)
    } else {
      NotificationManager.error('Could not post your review')
    }
    yield put(createReviewFailure(e.response.data));
  }
}

export function* deleteReview({payload: data}) {
  try {
    const response = yield axiosApi.delete('/reviews/' + data.review + '/' + data.place);
    yield put(deleteReviewSuccess(response.data));
    yield put(fetchReviewsRequest(data.place));
    yield put(fetchPlaceRequest(data.place));
    NotificationManager.success('Your review was deleted')
  } catch (e) {
    yield put(deleteReviewFailure());
    NotificationManager.error('Could not fetch cafes')
  }
}

const reviewsSagas = [
  takeEvery(fetchReviewsRequest, fetchReviews),
  takeEvery(createReviewRequest, createReview),
  takeEvery(deleteReviewRequest, deleteReview)
];

export default reviewsSagas;