import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import placesSlice from "./slices/placesSlice";
import reviewSlice from "./slices/reviewSlice";
import photoSlice from "./slices/photoSlice";


const rootReducer = combineReducers({
  users: usersSlice.reducer,
  places: placesSlice.reducer,
  reviews: reviewSlice.reducer,
  photos: photoSlice.reducer
});


export default rootReducer;