import {all} from 'redux-saga/effects';
import historySagas from "./sagas/historySagas";
import history from "../history";
import placesSagas from "./sagas/placesSagas";
import usersSagas from "./sagas/usersSagas";
import reviewsSagas from "./sagas/reviewsSagas";
import photoSagas from "./sagas/photoSagas";

export default function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...placesSagas,
    ...reviewsSagas,
    ...photoSagas
  ])
};