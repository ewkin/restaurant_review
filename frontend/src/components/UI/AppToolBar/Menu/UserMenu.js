import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Avatar, Grid, IconButton, makeStyles, Menu, MenuItem, Typography} from "@material-ui/core";
import {logoutRequest} from "../../../../store/actions/usersActions";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import {historyPush} from "../../../../store/actions/historyActions";

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
}));


const UserMenu = ({user}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  return (
    <>
      <IconButton
        color={'inherit'}
        onClick={handleClick}>
        <Grid container spacing={1} justify="space-between">
          <Grid item> {user.avatar ?
            <Avatar
              className={classes.avatar}
              alt="User avatar"
              sizes={'2'}
              src={user.avatar}/> : <Avatar className={classes.avatar}
            />}
          </Grid>
          <Grid item>
            <Typography variant="body1" display="block" gutterBottom>{user.displayName} </Typography>
          </Grid>
          <Grid item> <KeyboardArrowDownIcon/></Grid>
        </Grid>
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => {
          handleClose();
          dispatch(historyPush('/add_place'));
        }}>Add a place</MenuItem>
        <MenuItem onClick={() => dispatch(logoutRequest())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;