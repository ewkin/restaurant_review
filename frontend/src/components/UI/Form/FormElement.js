import React from 'react';
import PropTypes from 'prop-types';
import {Grid, TextField} from "@material-ui/core";

const FormElement = ({error, options, hide, size, ...props}) => {
  return (
    <Grid item xs={size ? size : 12}>
      <TextField
        error={Boolean(error)}
        helperText={error}
        {...props}
      />
    </Grid>
  );
};

FormElement.propTypes = {
  ...TextField.propTypes,
  error: PropTypes.string,
  select: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.object)
};

export default FormElement;